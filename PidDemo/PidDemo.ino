#include <Servo.h>
#include <PID_v1.h>
#include <SparkFunVKeyVoltageKeypad.h>

#define TIMESTEP 200

Servo servo;

double setpoint = 90;
double feedback = 0;
double power;

double kp = 0.05; //0.05;
double ki = 0; //0.02;
double kd = 0; //0.001;

PID pid(&feedback, &power, &setpoint, kp, ki, kd, DIRECT);
VKey keypad(A3, VKey::FIVE);

void setup() {
  pid.SetMode(AUTOMATIC);
  pid.SetSampleTime(TIMESTEP);
  pid.SetOutputLimits(-100, 100);
  
  Serial.begin(9600);
  
  pinMode(LED_BUILTIN, OUTPUT);

  servo.attach(3);

  // Delay for programming before inducing massive load on USB
  delay(3000);
  
  servo.write(0);

  delay(1000);
}

void loop() {
  pid.Compute();
  
  feedback += power;

  servo.write(feedback);

  Serial.print(power);
  Serial.print(", ");
  Serial.println(feedback);

  VKey::eKeynum key;

  boolean pressed = keypad.checkKeys(key);

  switch ((int)key) {
    case 1:
      kp = 4;
      break;
    case 4:
      kp = 1;
      break;
    case 7:
      kp = 0.1;
      break;
    case 10:
      kp = 0;
      break;
    case 2:
      servo.write(0);
      delay(1000);
      servo.write(90);
      delay(99999);
      break;
    case 5:
      ki = 1;
      break;
    case 8:
      ki = 0.01;
      break;
    case 11:
      ki = 0;
      break;
    case 3:
      kp = 0;
      ki = 0;
      kd = 0;
      break;
    case 6:
      kd = 0.25;
      break;
    case 9:
      kd = 0.025;
      break;
    case 12:
      kd = 0;
      break;
  }

  if (pressed) {
    Serial.print("Setting parameters to kp=");
    Serial.print(kp);
    Serial.print(", ki=");
    Serial.print(ki);
    Serial.print(", kd=");
    Serial.println(kd);
    pid.SetTunings(kp, ki, kd);
    feedback = 0;
    servo.write(0);
    delay(1000);
  }

  delay(TIMESTEP);
}

void blink() {
  digitalWrite(LED_BUILTIN, HIGH);
  delay(100);
  digitalWrite(LED_BUILTIN, LOW);
  delay(100);
}
